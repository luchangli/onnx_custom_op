import os
import numpy as np
import onnxruntime as onnxrt


def testRegisterCustomOpsLibrary():

    shared_library = 'build/libcustomop.so'
    if not os.path.exists(shared_library):
        raise FileNotFoundError("Unable to find '{0}'".format(shared_library))

    custom_op_model = "custom_op_test.onnx"

    if not os.path.exists(custom_op_model):
        raise FileNotFoundError("Unable to find '{0}'".format(custom_op_model))

    so1 = onnxrt.SessionOptions()
    so1.register_custom_ops_library(shared_library)

    # "CUDAExecutionProvider", "CPUExecutionProvider"
    available_providers = ["CUDAExecutionProvider"]

    # Model loading successfully indicates that the custom op node could be resolved successfully
    sess1 = onnxrt.InferenceSession(custom_op_model, sess_options=so1, providers=available_providers)
    #Run with input data
    input_name_0 = sess1.get_inputs()[0].name
    input_name_1 = sess1.get_inputs()[1].name
    output_name = sess1.get_outputs()[0].name
    input_0 = np.ones((3,5)).astype(np.float32)
    input_1 = np.ones((3,5)).astype(np.float32)
    res = sess1.run([output_name], {input_name_0: input_0, input_name_1: input_1})

    print("exec output:", res)

    # output_expected = 2*np.ones((3,5)).astype(np.float32)
    # np.testing.assert_allclose(output_expected, res[0], rtol=1e-05, atol=1e-08)


testRegisterCustomOpsLibrary()