#include "custom_op_trt.h"

void KernelOne::Compute(OrtKernelContext* context) {
  // Setup inputs
  const OrtValue* input_X = ort_.KernelContext_GetInput(context, 0);
  const OrtValue* input_Y = ort_.KernelContext_GetInput(context, 1);
  const float* X = ort_.GetTensorData<float>(input_X);
  const float* Y = ort_.GetTensorData<float>(input_Y);

  // Setup output
  OrtTensorDimensions dimensions(ort_, input_X);

  OrtValue* output = ort_.KernelContext_GetOutput(context, 0, dimensions.data(), dimensions.size());
  float* out = ort_.GetTensorMutableData<float>(output);

  OrtTensorTypeAndShapeInfo* output_info = ort_.GetTensorTypeAndShape(output);
  int64_t size = ort_.GetTensorShapeElementCount(output_info);
  ort_.ReleaseTensorTypeAndShapeInfo(output_info);

  cudaStream_t stream = reinterpret_cast<cudaStream_t>(ort_.KernelContext_GetGPUComputeStream(context));

  std::cout << "stream:" << (stream == nullptr) << std::endl;

  cuda_add(size, out, X, Y, stream);
}

ModelInfo trtModelInf;

void* CustomOpTRT::CreateKernel(OrtApi api_, const OrtKernelInfo* info) const {
  Ort::CustomOpApi ort_(api_);

  std::string model_info = ort_.KernelInfoGetAttribute<std::string>(info, "_dlengine_trt_model_info");

  printf("model_info:%s\n", model_info.c_str());

  trtModelInf.input_num = 1;
  trtModelInf.output_num = 1;

  printf("call CreateKernel\n");

  return new KernelOne(api_);
};

size_t CustomOpTRT::GetInputTypeCount() const {

  printf("call GetInputTypeCount\n");

  return trtModelInf.input_num;
};

// get input dtype for idx'th input
ONNXTensorElementDataType CustomOpTRT::GetInputType(size_t index) const {
  return ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT;
};

size_t CustomOpTRT::GetOutputTypeCount() const {

  printf("call GetOutputTypeCount\n");

  return trtModelInf.output_num;
};

// get output dtype for idx'th output
ONNXTensorElementDataType CustomOpTRT::GetOutputType(size_t index) const {
  return ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT;
};
