#include "custom_op_reg.h"
#include <cuda_runtime.h>
#include "onnxruntime/core/graph/constants.h"

template <typename T1, typename T2, typename T3>
void cuda_add(int64_t, T3*, const T1*, const T2*, cudaStream_t compute_stream);

struct KernelOne {
  KernelOne(OrtApi api)
    : api_(api),
      ort_(api_) {
  }

  void Compute(OrtKernelContext* context);

 private:
  OrtApi api_;  // keep a copy of the struct, whose ref is used in the ort_
  Ort::CustomOpApi ort_;
};


struct ModelInfo {
  int input_num = 0;
  int output_num = 0;
};


struct CustomOpTRT : Ort::CustomOpBase<CustomOpTRT, KernelOne> {
  const char* GetName() const {
    return "CustomOpTRT";
  };

  const char* GetExecutionProviderType() const {
    return onnxruntime::kCudaExecutionProvider;
  };

  void* CreateKernel(OrtApi api_, const OrtKernelInfo* info) const;

  size_t GetInputTypeCount() const;

  // get input dtype for idx'th input
  ONNXTensorElementDataType GetInputType(size_t index) const;

  size_t GetOutputTypeCount() const;

  // get output dtype for idx'th output
  ONNXTensorElementDataType GetOutputType(size_t index) const;

};
