# ******************************************************************************
# Copyright 2018-2020 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ******************************************************************************
import json
import onnx
import numpy as np
from onnx.helper import make_node, make_graph, make_tensor_value_info, make_model, make_opsetid
from onnx.mapping import TENSOR_TYPE_TO_NP_TYPE


def get_np_2_onnx_dtype_map():
    dtype_map = {}
    for key, val in TENSOR_TYPE_TO_NP_TYPE.items():
        if val not in dtype_map:
            dtype_map[val] = key
    return dtype_map


model_info_txt = '{"model_path": "", "input_names": ["input"], "input_shapes": [[1, 3, 224, 224]], "input_dtypes": ["float32"], "output_names": ["resnet_model/stage_1/Relu_2"], "min_shapes": null, "opt_shapes": null, "max_shapes": null, "trt_version": [8, 2, 1, 8], "quant_type": "fp32", "dynamic_shape": false, "workspace_size_mb": 2048, "output_shapes": [[1, 256, 56, 56]], "output_dtypes": ["float32"]}'
model_info = json.loads(model_info_txt)

print(model_info)

np_2_onnx_dtype_map = get_np_2_onnx_dtype_map()
print("np_2_onnx_dtype_map:", np_2_onnx_dtype_map)


input_names = model_info["input_names"]
input_shapes = model_info["input_shapes"]
input_dtypes = model_info["input_dtypes"]

output_names = model_info["output_names"]
output_shapes = model_info["output_shapes"]
output_dtypes = model_info["output_dtypes"]

input_dtypes_onnx = [np_2_onnx_dtype_map[np.dtype(dtype_str)] for dtype_str in input_dtypes]
output_dtypes_onnx = [np_2_onnx_dtype_map[np.dtype(dtype_str)] for dtype_str in output_dtypes]

print("input_dtypes_onnx:", input_dtypes_onnx)
print("output_dtypes_onnx:", output_dtypes_onnx)

input_num = len(input_names)
output_num = len(output_names)

input_tensors_value = [make_tensor_value_info(
    input_names[i], input_dtypes_onnx[i], input_shapes[i]) for i in range(input_num)]
output_tensors_value = [make_tensor_value_info(
    output_names[i], output_dtypes_onnx[i], output_shapes[i]) for i in range(output_num)]


def make_onnx_model_for_custom_op(model_info, domain="", opset=11):

    node = make_node('CustomOpTRT', input_names, output_names, name='CustomOpTRT', domain=domain)
    model_info_attr = onnx.helper.make_attribute("_dlengine_trt_model_info", model_info_txt)
    node.attribute.append(model_info_attr)

    graph = make_graph([node], 'fused_graph', input_tensors_value, output_tensors_value)
    model = make_model(graph, producer_name='dl_engine', opset_imports=[make_opsetid(domain=domain, version=opset)])

    return model


onnx_model = make_onnx_model_for_custom_op(model_info)
# onnx.checker.check_model(onnx_model)

onnx.save(onnx_model, "custom_op_trt.onnx")
