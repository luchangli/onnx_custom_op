#!/bin/bash

# download codes to get header files to compile custom op
ORT_VERSION=$(python3 -c 'import onnxruntime as onnxrt; print(onnxrt.__version__)')
wget --no-check-certificate https://github.com/microsoft/onnxruntime/archive/refs/tags/v${ORT_VERSION}.tar.gz
