# ******************************************************************************
# Copyright 2018-2020 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ******************************************************************************
import onnx
import numpy as np
from onnx.helper import make_node, make_graph, make_tensor_value_info, make_model, make_opsetid

domain=""

def make_onnx_model_for_matmul_op(input_left, input_right):
    output_shape = input_left.shape

    node = make_node('CustomOpOne', ['X', 'Y'], ['Z'], name='test_node', domain=domain)

    graph = make_graph([node], 'test_graph',
                       [make_tensor_value_info('X', onnx.TensorProto.FLOAT, input_left.shape),
                        make_tensor_value_info('Y', onnx.TensorProto.FLOAT, input_right.shape)],
                       [make_tensor_value_info('Z', onnx.TensorProto.FLOAT, output_shape)])
    model = make_model(graph, producer_name='ngraph ONNXImporter', opset_imports=[make_opsetid(domain=domain, version=12)])
    return model


def import_and_compute_matmul(left_shape, right_shape):
    input_data_left = np.random.rand(*left_shape)
    input_data_right = np.random.rand(*right_shape)
    onnx_model = make_onnx_model_for_matmul_op(input_data_left, input_data_right)
    return onnx_model


data = ([3, 5], [3, 5])
onnx_model = import_and_compute_matmul(*data)
# onnx.checker.check_model(onnx_model)

onnx.save(onnx_model, "custom_op1.onnx")

